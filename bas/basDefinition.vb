﻿Imports System.Data
Imports Oracle.DataAccess.Client
Imports Npgsql

Module basDefinition
    ''' <summary>
    ''' 戻り値　正常・・・0
    ''' </summary>
    Public Const RET_NORMAL As Integer = 0
    ''' <summary>
    ''' 戻り値　データなし・・・-1
    ''' </summary>
    Public Const RET_NOTFOUND As Integer = -1
    ''' <summary>
    ''' 戻り値　ワーニング・・・-2
    ''' </summary>
    Public Const RET_WARNING As Integer = -2
    ''' <summary>
    ''' 戻り値　エラー・・・-9
    ''' </summary>
    Public Const RET_ERROR As Integer = -9

    ''' <summary>
    ''' Oracle接続用
    ''' </summary>
    Public connOra As New OracleConnection

    ''' <summary>
    ''' Postgret接続用
    ''' </summary>
    Public connPostgret As New NpgsqlConnection

    ''' <summary>
    ''' 上位システム用患者情報構造体
    ''' </summary>
    Public Structure TYPE_GET_PATIENT
        Public PATIENTNO As String
        Public SEX As String
        Public BIRTHDAY As String
        Public REFLASTNAME As String
        Public REFFIRSTNAME As String
        Public REFMIDLLENAME As String
        Public KANALASTNAME As String
        Public KANAFIRSTNAME As String
        Public KANAMIDDLENAME As String
        Public KANJILASTNAME As String
        Public KANJIFIRSTNAME As String
        Public KANJIMIDDLENAME As String
    End Structure

    ''' <summary>
    ''' 自DB用患者情報構造体
    ''' </summary>
    Public Structure TYPE_UPD_PATIENT
        Public PATIENTID As String
        Public NAME As String
        Public NAMEKANA As String
        Public BIRTH As String
        Public GENDERID As String
    End Structure
End Module
