﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' アセンブリに関する一般情報は以下の属性セットをとおして制御されます。
' アセンブリに関連付けられている情報を変更するには、
' これらの属性値を変更してください。

' アセンブリ属性の値を確認します

<Assembly: AssemblyTitle("PatientIF")>
<Assembly: AssemblyDescription("患者情報連携アプリケーション")>
<Assembly: AssemblyCompany("プライアルメディカルシステム 株式会社")>
<Assembly: AssemblyProduct("PatientIF")>
<Assembly: AssemblyCopyright("Copyright ©  2020")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'このプロジェクトが COM に公開される場合、次の GUID が typelib の ID になります
<Assembly: Guid("6f8b2211-930d-4092-8eee-069b1fe4faa8")>

' アセンブリのバージョン情報は次の 4 つの値で構成されています:
'
'      メジャー バージョン
'      マイナー バージョン
'      ビルド番号
'      Revision
'
' すべての値を指定するか、次を使用してビルド番号とリビジョン番号を既定に設定できます
' 既定値にすることができます:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.5.0")>
<Assembly: AssemblyFileVersion("1.0.5.0")>
