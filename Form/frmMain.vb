﻿Imports Npgsql

Public Class frmMain
    Private aryGetStaff() As TYPE_GET_PATIENT
    Private typUpdStaff As TYPE_UPD_PATIENT

#Region "Form Events"
    ''' <summary>
    ''' FormLoad
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        '---タイトル表示
        Me.Text = "患者情報連携アプリケーション" & Space(1) &
                  "Ver." & My.Application.Info.Version.Major &
                  "." & My.Application.Info.Version.Minor &
                  "." & My.Application.Info.Version.Build

        '---リストボックスの初期化
        Me.lstMsg.Items.Clear()

        '---リストボックス表示
        Call subDispList(0, "患者情報連携アプリケーション 起動", strMethod)

        '---ログ出力
        Call subOutLog(0, "患者情報連携アプリケーション 起動", strMethod)

        Me.tmrProc.Interval = My.Settings.Interval
        Me.tmrProc.Enabled = True
    End Sub
#End Region

#Region "ControlEvents"
    ''' <summary>
    ''' 閉じるボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        '---ログ出力
        Call subOutLog(0, "患者情報連携アプリケーション 終了", strMethod)

        End
    End Sub

    ''' <summary>
    ''' 処理タイマー
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub tmrProc_Tick(sender As Object, e As EventArgs) Handles tmrProc.Tick
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Me.tmrProc.Enabled = False

        If fncGetStaff() = RET_ERROR Then
            '---リストボックス表示
            Call subDispList(RET_ERROR, "患者情報登録/更新エラー：患者ID=" & typUpdStaff.PATIENTID, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "患者情報登録/更新エラー：患者ID=" & typUpdStaff.PATIENTID, strMethod)
        End If

        End

        Me.tmrProc.Enabled = True
    End Sub
#End Region

#Region "SubRoutine"

    ''' <summary>
    ''' リストボックス表示
    ''' </summary>
    ''' <param name="Proc">0・・・Inf、2・・・War、-9・・・Err</param>
    ''' <param name="msg">出力メッセージ</param>
    ''' <param name="strMethod">発生場所</param>
    Private Sub subDispList(ByVal Proc As Integer, ByVal msg As String, ByVal strMethod As String)
        Dim strOutMsg As String = vbNullString
        Dim strYYYY As String = Now.Year.ToString
        Dim strMM As String = Now.Month.ToString("0#")
        Dim strDD As String = Now.Day.ToString("0#")
        Dim strDateTime As String = Now.ToString("yyyy/MM/dd HH:mm:ss")

        Select Case Proc
            Case RET_NORMAL
                strOutMsg = "[INF](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_WARNING
                strOutMsg = "[WAR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_ERROR
                strOutMsg = "[ERR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
        End Select

        If Me.lstMsg.Items.Count > My.Settings.MaxListRow Then
            Me.lstMsg.Items.Clear()
        End If

        Me.lstMsg.Items.Add(strOutMsg)

        Me.lstMsg.SelectedIndex = Me.lstMsg.Items.Count - 1

        Me.lstMsg.Refresh()
    End Sub

    ''' <summary>
    ''' 患者情報取得
    ''' </summary>
    ''' <returns></returns>
    Private Function fncGetStaff() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim dt As New DataTable
        Dim intCounter As Integer = 0
        Dim intProcInterval As Integer = 0

        Try
            '---リストボックス表示
            Call subDispList(0, "----- 患者情報取得動(Oracle) -----", strMethod)
            '---ログ出力
            Call subOutLog(0, "----- 患者情報取得動(Oracle) -----", strMethod)

            '---処理間隔の退避
            intProcInterval = My.Settings.ProcInterval

            '---SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT")
            strSQL.AppendLine("PATIENTNO,")
            strSQL.AppendLine("NVL(SEX,'') AS SEX,")
            strSQL.AppendLine("NVL(BIRTHDAY,'') AS BIRTHDAY,")
            strSQL.AppendLine("NVL(KANJIFIRSTNAME,'') AS KANJIFIRSTNAME,")
            strSQL.AppendLine("NVL(KANJIMIDDLENAME,'') AS KANJIMIDDLENAME,")
            strSQL.AppendLine("NVL(KANJILASTNAME,'') AS KANJILASTNAME,")
            strSQL.AppendLine("NVL(KANAFIRSTNAME,'') AS KANAFIRSTNAME,")
            strSQL.AppendLine("NVL(KANAMIDDLENAME,'') AS KANAMIDDLENAME,")
            strSQL.AppendLine("NVL(KANALASTNAME,'') AS KANALASTNAME")
            strSQL.AppendLine("FROM MRIBAPATIENT")

            '---Ver.1.0.2 取得条件で「当日4時間前」を時間をConfigで設定可能にした。UpDated Watanabe 2020.12.23
            'strSQL.AppendLine("WHERE (IF_CREATEDATE >= (SELECT SYSDATE - 4/24 FROM DUAL))")
            'strSQL.AppendLine("OR (IF_UPDATEDATE >= (SELECT SYSDATE - 4/24 FROM DUAL))")
            If intProcInterval > 0 Then
                '---ProcIntervalに設定された時間前のデータを対象とする
                strSQL.AppendLine("WHERE (IF_CREATEDATE >= (SELECT SYSDATE - " & intProcInterval & "/24 FROM DUAL))")
                strSQL.AppendLine("OR (IF_UPDATEDATE >= (SELECT SYSDATE - " & intProcInterval & "/24 FROM DUAL))")
            Else
                '---ProcIntervalが0以下は全データ対象とする。
            End If

            '---データ取得
            If DBQueryOracle(strSQL.ToString, dt) = RET_ERROR Then
                Call subDispList(RET_ERROR, "患者情報検索(Oracle)エラー", strMethod)
                Return RET_ERROR
            End If

            '---取得結果0件
            If dt.Rows.Count = 0 Then
                Call subDispList(RET_NOTFOUND, "対象患者情報(Oracle)はありませんでした。", strMethod)
                Return RET_NOTFOUND
                Exit Function
            End If

            Call subDispList(RET_NORMAL, "対象患者情報(Oracle)件数 = " & dt.Rows.Count & " 件", strMethod)

            '---構造体の初期化
            Erase aryGetStaff

            '---取得データ構造体退避
            For intCounter = 0 To dt.Rows.Count - 1

                System.Windows.Forms.Application.DoEvents()

                ReDim Preserve aryGetStaff(intCounter)

                With aryGetStaff(intCounter)
                    '---Ver.1.0.4.0 DBNULL対応 UpDated By Watanabe 2021.05.14
                    '.PATIENTNO = dt.Rows(intCounter).Item("PATIENTNO").ToString                                                                             '患者ID
                    .PATIENTNO = IIf(IsDBNull(dt.Rows(intCounter).Item("PATIENTNO")), "", dt.Rows(intCounter).Item("PATIENTNO").ToString)                   '患者ID
                    .SEX = IIf(IsDBNull(dt.Rows(intCounter).Item("SEX")), "", dt.Rows(intCounter).Item("SEX").ToString)                                     '性別
                    .BIRTHDAY = IIf(IsDBNull(dt.Rows(intCounter).Item("BIRTHDAY")), "", dt.Rows(intCounter).Item("BIRTHDAY").ToString)                      '生年月日
                    .KANJIFIRSTNAME = IIf(IsDBNull(dt.Rows(intCounter).Item("KANJIFIRSTNAME")), "", dt.Rows(intCounter).Item("KANJIFIRSTNAME").ToString)    '漢字名
                    .KANJIMIDDLENAME = IIf(IsDBNull(dt.Rows(intCounter).Item("KANJIMIDDLENAME")), "", dt.Rows(intCounter).Item("KANJIMIDDLENAME").ToString) '漢字ミドルネーム
                    .KANJILASTNAME = IIf(IsDBNull(dt.Rows(intCounter).Item("KANJILASTNAME")), "", dt.Rows(intCounter).Item("KANJILASTNAME").ToString)       '漢字姓
                    .KANAFIRSTNAME = IIf(IsDBNull(dt.Rows(intCounter).Item("KANAFIRSTNAME")), "", dt.Rows(intCounter).Item("KANAFIRSTNAME").ToString)       'カナ名
                    .KANAMIDDLENAME = IIf(IsDBNull(dt.Rows(intCounter).Item("KANAMIDDLENAME")), "", dt.Rows(intCounter).Item("KANAMIDDLENAME").ToString)    'カナミドルネーム 
                    .KANALASTNAME = IIf(IsDBNull(dt.Rows(intCounter).Item("KANALASTNAME")), "", dt.Rows(intCounter).Item("KANALASTNAME").ToString)          'カナ姓

                    Call subDispList(RET_NORMAL, "データ取得：患者ID=" & .PATIENTNO & ",カナ氏名=" & .KANAFIRSTNAME & .KANAMIDDLENAME & .KANALASTNAME, strMethod)
                End With

                '---更新データ構造体退避
                typUpdStaff = Nothing
                typUpdStaff = New TYPE_UPD_PATIENT

                With typUpdStaff
                    .PATIENTID = aryGetStaff(intCounter).PATIENTNO
                    .NAME = aryGetStaff(intCounter).KANJILASTNAME.Trim & Space(1) &
                            aryGetStaff(intCounter).KANJIMIDDLENAME.Trim & Space(1) &
                            aryGetStaff(intCounter).KANJIFIRSTNAME.Trim
                    .NAMEKANA = aryGetStaff(intCounter).KANALASTNAME.Trim & Space(1) &
                                aryGetStaff(intCounter).KANAMIDDLENAME.Trim & Space(1) &
                                aryGetStaff(intCounter).KANAFIRSTNAME.Trim
                    '---START Ver.1.0.5.0 生年月日がブランクを想定  UpDated By Watanabe 2021.05.20
                    '.BIRTH = Replace(aryGetStaff(intCounter).BIRTHDAY.ToString, "/", "")
                    If aryGetStaff(intCounter).BIRTHDAY.ToString <> "" Then
                        .BIRTH = Replace(aryGetStaff(intCounter).BIRTHDAY.ToString, "/", "")
                    End If
                    '---END Ver.1.0.5.0 生年月日がブランクを想定  UpDated By Watanabe 2021.05.20
                    Select Case aryGetStaff(intCounter).SEX
                        Case 0
                            .GENDERID = "1"
                        Case 1
                            .GENDERID = "2"
                        Case Else
                            .GENDERID = "0"
                    End Select

                End With

                If fncUpdPatient(typUpdStaff) = RET_ERROR Then
                    '---リストボックス表示
                    Call subDispList(RET_ERROR, "患者情報登録/更新エラー：患者ID=" & typUpdStaff.PATIENTID, strMethod)
                    '---ログ出力
                    Call subOutLog(RET_ERROR, "患者情報登録/更新エラー：患者ID=" & typUpdStaff.PATIENTID, strMethod)

                    Continue For
                End If
            Next intCounter

            Return RET_NORMAL

        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally
            dt.Dispose()
            dt = Nothing

        End Try
    End Function

    ''' <summary>
    ''' 患者情報登録/更新
    ''' </summary>
    ''' <returns></returns>
    Private Function fncUpdPatient(ByVal objPatient As TYPE_UPD_PATIENT) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim objData As New d_prim_manurtyDataSetTableAdapters.PatientsTableAdapter
        Dim dt As New d_prim_manurtyDataSet.PatientsDataTable
        Dim strSQL As New System.Text.StringBuilder
        Dim cmd As NpgsqlCommand

        Try
            objData.FillByPatientID(dt, objPatient.PATIENTID)

            strSQL.Clear()

            If dt.Rows.Count = 0 Then
                strSQL.AppendLine("INSERT INTO ""Patients""")
                strSQL.AppendLine("(")
                strSQL.AppendLine("""patientId"",")
                strSQL.AppendLine("""name"",")
                strSQL.AppendLine("""nameKana"",")
                strSQL.AppendLine("""birth"",")
                strSQL.AppendLine("""createdAt"",")
                strSQL.AppendLine("""updatedAt"",")
                strSQL.AppendLine("""GenderId""")
                strSQL.AppendLine(")")
                strSQL.AppendLine("VALUES")
                strSQL.AppendLine("(")
                strSQL.AppendLine("'" & objPatient.PATIENTID & "',")
                strSQL.AppendLine("'" & objPatient.NAME & "',")
                strSQL.AppendLine("'" & objPatient.NAMEKANA & "',")
                strSQL.AppendLine("'" & objPatient.BIRTH & "',")
                strSQL.AppendLine("'" & Date.Now & "',")
                strSQL.AppendLine("'" & Date.Now & "',")
                strSQL.AppendLine("'" & objPatient.GENDERID & "'")
                strSQL.AppendLine(")")
            Else
                strSQL.AppendLine("UPDATE ""Patients""")
                strSQL.AppendLine("SET")
                strSQL.AppendLine("""name"" = '" & objPatient.NAME & "',")
                strSQL.AppendLine("""nameKana"" = '" & objPatient.NAMEKANA & "',")
                strSQL.AppendLine("""birth"" = '" & objPatient.BIRTH & "',")
                strSQL.AppendLine("""updatedAt"" = '" & Date.Now & "',")
                strSQL.AppendLine("""GenderId"" = " & objPatient.GENDERID & "")
                strSQL.AppendLine("WHERE ")
                strSQL.AppendLine("""patientId"" = '" & objPatient.PATIENTID & "'")
            End If

            '---生成SQL文をログ出力
            Call subOutLog(RET_NORMAL, "SQL：" & strSQL.ToString, strMethod)

            '---Postgret接続
            Call ConnectionOpenPostgret()

            cmd = New NpgsqlCommand(strSQL.ToString, connPostgret)
            cmd.ExecuteNonQuery()

            cmd.Dispose()

            '---Postgret切断
            Call ConnectionClosePostgret()

            '---生成SQL文をログ出力
            Call subOutLog(RET_NORMAL, "----- 患者情報更新完了 -----", strMethod)
            Return RET_NORMAL
        Catch ex As Exception
            '---リストボックス表示
            Call subDispList(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            '---ログ出力
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)

            Return RET_ERROR
        Finally
            dt.Dispose()
            dt = Nothing
            objData.Dispose()
            cmd.Dispose()
        End Try
    End Function

#End Region

End Class
